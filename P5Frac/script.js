const size_width = 760;
const size_height = 540;

let angle = 0;

var typ = "default";
var fromConstCA = "";
var fromConstCB = "";

// Maximale Anzahl an Iterationen für jeden Punkt auf der komplexen Ebene
const maxiterations = 100;

// Farben für jede mögliche Iteration
const colorsRed = [];
const colorsGreen = [];
const colorsBlue = [];

function setup() {
  pixelDensity(1);
  createCanvas(size_width, size_height);
  colorMode(HSB, 1);

  // Farben erstellen, die für jede Iteration genutzt wird
  for (let n = 0; n < maxiterations; n++) {
    let hu = sqrt(n / maxiterations);
    let col = color(hu, 255, 150);
    colorsRed[n] = green(col);
    colorsGreen[n] = blue(col);
    colorsBlue[n] = red(col);
  }
}

function draw() {
  if(typ == "interaktiv" || typ == "default") {
    let ca = map(mouseX, 0, width, -1, 1); //-0.70176;
    let cb = map(mouseY, 0, height, -1, 1); //-0.3842;

    background(255);

    // Erstelle einen Wertebereich von Werten auf der komplexen Ebene
    // Ein unterschiedlicher Bereich lässt einen "Zoom" in das Fraktal zu

    // Die Breite ist sozusagen der Zoom-Faktor, da die anderen Werte eben abhängig von der Breite sind
    // Ein höherer Breitenwert zoomt in das Fraktal rein, ein niedrigerer Breitenwert zoomt aus dem Fraktal raus

    //let w = abs(sin(angle)) * 5;
    let w = 5;
    let h = (w * height) / width;

    // Beginne an der negativen Hälfte der Breite und Höhe
    let xmin = -w / 2;
    let ymin = -h / 2;

    // Wichtig ist es, alle Pixel in das Piexels-Array (pixels[]) zu schreiben
    // Das ist nur einmal notwendig mit "loadPixels()"
    loadPixels();

    // x reicht von xmin zu xmax
    let xmax = xmin + w;
    // y reicht von ymin zu ymax
    let ymax = ymin + h;

    // Hier wird die Anzahl berechnet, um wie viel x und y pro Pixel erhöht wird
    let dx = (xmax - xmin) / width;
    let dy = (ymax - ymin) / height;

    // Beginn y
    let y = ymin;
    for (let j = 0; j < height; j++) {
      // Beginn x
      let x = xmin;
      for (let i = 0; i < width; i++) {
        // Nun das ganze testen, solange z = z^2 + cm iteriert, läuft z gegen Unendlich?
        let a = x;
        let b = y;
        let n = 0;
        while (n < maxiterations) {
          let aa = a * a;
          let bb = b * b;
          // In unserer Realität ist Unendlich einfach Unendlich, aber Unendlich kann man nicht wirklich darstellen, 
          // also wird hier einfach mal die 16 betrachtet und als "unendlich" definiert.
          if (aa + bb > 4.0) {
            break; // "Unendlich erreicht"
          }
          let twoab = 2.0 * a * b;
          a = aa - bb + ca;
          b = twoab + cb;
          n++;
        }

        // Jeder Pixel wird angemalt, basiert auf die Dauer, bis "unendlich" erreicht wird
        // Falls das nie geschieht, wird die Farbe Schwarz gewählt
        let pix = (i + j * width) * 4;
        if (n == maxiterations) {
          pixels[pix + 0] = 0;
          pixels[pix + 1] = 0;
          pixels[pix + 2] = 0;
        } else {
          // Ansonsten werden die Farben genommen, die in "setup()" definiert wurden
          pixels[pix + 0] = colorsRed[n];
          pixels[pix + 1] = colorsGreen[n];
          pixels[pix + 2] = colorsBlue[n];
        }
        x += dx;
      }
      y += dy;
    }
    updatePixels();
  } else if(typ == "automatic") {
    let ca = cos(angle * 3.213); //sin(angle);
    let cb = sin(angle);

    angle += 0.02;

    background(255);

    // Erstelle einen Wertebereich von Werten auf der komplexen Ebene
    // Ein unterschiedlicher Bereich lässt einen "Zoom" in das Fraktal zu

    // Die Breite ist sozusagen der Zoom-Faktor, da die anderen Werte eben abhängig von der Breite sind
    // Ein höherer Breitenwert zoomt in das Fraktal rein, ein niedrigerer Breitenwert zoomt aus dem Fraktal raus

    //let w = abs(sin(angle)) * 5;
    let w = 5;
    let h = (w * height) / width;

    // Beginne an der negativen Hälfte der Breite und Höhe
    let xmin = -w / 2;
    let ymin = -h / 2;

    // Wichtig ist es, alle Pixel in das Piexels-Array (pixels[]) zu schreiben
    // Das ist nur einmal notwendig mit "loadPixels()"
    loadPixels();

    // x reicht von xmin zu xmax
    let xmax = xmin + w;
    // y reicht von ymin zu ymax
    let ymax = ymin + h;

    // Hier wird die Anzahl berechnet, um wie viel x und y pro Pixel erhöht wird
    let dx = (xmax - xmin) / width;
    let dy = (ymax - ymin) / height;

    // Beginn y
    let y = ymin;
    for (let j = 0; j < height; j++) {
      // Beginn x
      let x = xmin;
      for (let i = 0; i < width; i++) {
        // Nun das ganze testen, solange z = z^2 + cm iteriert, läuft z gegen Unendlich?
        let a = x;
        let b = y;
        let n = 0;
        while (n < maxiterations) {
          let aa = a * a;
          let bb = b * b;
          // In unserer Realität ist Unendlich einfach Unendlich, aber Unendlich kann man nicht wirklich darstellen, 
          // also wird hier einfach mal die 16 betrachtet und als "unendlich" definiert.
          if (aa + bb > 4.0) {
            break; // "unendlich erreicht"
          }
          let twoab = 2.0 * a * b;
          a = aa - bb + ca;
          b = twoab + cb;
          n++;
        }

        // Jeder Pixel wird angemalt, basiert auf die Dauer, bis "unendlich" erreicht wird
        // Falls das nie geschieht, wird die Farbe Schwarz gewählt
        let pix = (i + j * width) * 4;
        if (n == maxiterations) {
          pixels[pix + 0] = 0;
          pixels[pix + 1] = 0;
          pixels[pix + 2] = 0;
        } else {
          // Ansonsten werden die Farben genommen, die in "setup()" definiert wurden
          pixels[pix + 0] = colorsRed[n];
          pixels[pix + 1] = colorsGreen[n];
          pixels[pix + 2] = colorsBlue[n];
        }
        x += dx;
      }
      y += dy;
    }
    updatePixels();
  } else if(typ == "constant") {
    if(fromConstCA == "" || fromConstCB == "") {
      var ca = 0//-0.70176;
      var cb = 0//-0.3842;
    } else {
      var ca = float(fromConstCA)//-0.70176;
      var cb = float(fromConstCB)//-0.3842;
    }

    angle += 0.02;

    background(255);
    
    // Erstelle einen Wertebereich von Werten auf der komplexen Ebene
    // Ein unterschiedlicher Bereich lässt einen "Zoom" in das Fraktal zu

    // Die Breite ist sozusagen der Zoom-Faktor, da die anderen Werte eben abhängig von der Breite sind
    // Ein höherer Breitenwert zoomt in das Fraktal rein, ein niedrigerer Breitenwert zoomt aus dem Fraktal raus

    //let w = abs(sin(angle)) * 5;
    let w = 5;
    let h = (w * height) / width;

    // Beginne an der negativen Hälfte der Breite und Höhe
    let xmin = -w / 2;
    let ymin = -h / 2;

    // Wichtig ist es, alle Pixel in das Piexels-Array (pixels[]) zu schreiben
    // Das ist nur einmal notwendig mit "loadPixels()"
    loadPixels();

    // x reicht von xmin zu xmax
    let xmax = xmin + w;
    // y reicht von ymin zu ymax
    let ymax = ymin + h;

    // Hier wird die Anzahl berechnet, um wie viel x und y pro Pixel erhöht wird
    let dx = (xmax - xmin) / width;
    let dy = (ymax - ymin) / height;

    // Beginn y
    let y = ymin;
    for (let j = 0; j < height; j++) {
      // Beginn x
      let x = xmin;
      for (let i = 0; i < width; i++) {
        // Nun das ganze testen, solange z = z^2 + cm iteriert, läuft z gegen Unendlich?
        let a = x;
        let b = y;
        let n = 0;
        while (n < maxiterations) {
          let aa = a * a;
          let bb = b * b;
          // In unserer Realität ist Unendlich einfach Unendlich, aber Unendlich kann man nicht wirklich darstellen, 
          // also wird hier einfach mal die 16 betrachtet und als "unendlich" definiert.
          if (aa + bb > 4.0) {
            break; // "unendlich erreicht"
          }
          let twoab = 2.0 * a * b;
          a = aa - bb + ca;
          b = twoab + cb;
          n++;
        }

        // Jeder Pixel wird angemalt, basiert auf die Dauer, bis "unendlich" erreicht wird
        // Falls das nie geschieht, wird die Farbe Schwarz gewählt
        let pix = (i + j * width) * 4;
        if (n == maxiterations) {
          pixels[pix + 0] = 0;
          pixels[pix + 1] = 0;
          pixels[pix + 2] = 0;
        } else {
          // Ansonsten werden die Farben genommen, die in "setup()" definiert wurden
          pixels[pix + 0] = colorsRed[n];
          pixels[pix + 1] = colorsGreen[n];
          pixels[pix + 2] = colorsBlue[n];
        }
        x += dx;
      }
      y += dy;
    }
    updatePixels();
  }
}

/**
 * Diese Funktion ist zur Bestimmung der Benutzerinteraktion definiert.
 * Je nachdem welche Auswahl vom Benutzer getroffen wird, so wird auch
 * die Anzeige geändert.
 * 
 * @param {*} type : Grundsätzlich akzeptiert wird ein Wert aus ["interaktiv", "automatic", "constant"]
 */
function drawType(type) {
  if(type == "interaktiv") {
    typ = type;
  } else if(type == "automatic") {
    typ = type;
  } else if(type == "constant") {
    fromConstCA = document.getElementById("input1").value;
    fromConstCB = document.getElementById("input2").value;
    typ = type;
  }
}

/**
 * Diese Funktion ist zur Bestimmung der Real-Konstante und Imaginär-Konstante definiert
 * Ein Klick auf die jeweilige Typen-Spalte interpretiert den Tabelleninhalt in die Input-Felder
 * 
 * @param {*} type : Grundsätzlich akzeptiert wird ein Wert aus ["typ1", "typ2", "typ3", "typ4", "typ5", "typ6", "typ7", "typ8", "typ9"]
 */
function loadConstant(type) {
  switch(type) {
    case "typ1":
      var r1 = document.getElementById("r1").textContent;
      var i1 = document.getElementById("i1").textContent;

      document.getElementById("input1").value = float(r1);
      document.getElementById("input2").value = float(i1);
      drawType("constant");
      break;
    case "typ2":
      var r2 = document.getElementById("r2").textContent;
      var i2 = document.getElementById("i2").textContent;

      document.getElementById("input1").value = float(r2);
      document.getElementById("input2").value = float(i2);
      drawType("constant");
      break;
    case "typ3":
      var r3 = document.getElementById("r3").textContent;
      var i3 = document.getElementById("i3").textContent;

      document.getElementById("input1").value = float(r3);
      document.getElementById("input2").value = float(i3);
      drawType("constant");
      break;
    case "typ4":
      var r4 = document.getElementById("r4").textContent;
      var i4 = document.getElementById("i4").textContent;

      document.getElementById("input1").value = float(r4);
      document.getElementById("input2").value = float(i4);
      drawType("constant");
      break;
    case "typ5":
      var r5 = document.getElementById("r5").textContent;
      var i5 = document.getElementById("i5").textContent;

      document.getElementById("input1").value = float(r5);
      document.getElementById("input2").value = float(i5);
      drawType("constant");
      break;
    case "typ6":
      var r6 = document.getElementById("r6").textContent;
      var i6 = document.getElementById("i6").textContent;

      document.getElementById("input1").value = float(r6);
      document.getElementById("input2").value = float(i6);
      drawType("constant");
      break;
    case "typ7":
      var r7 = document.getElementById("r7").textContent;
      var i7 = document.getElementById("i7").textContent;

      document.getElementById("input1").value = float(r7);
      document.getElementById("input2").value = float(i7);
      drawType("constant");
      break;
    case "typ8":
      var r8 = document.getElementById("r8").textContent;
      var i8 = document.getElementById("i8").textContent;

      document.getElementById("input1").value = float(r8);
      document.getElementById("input2").value = float(i8);
      drawType("constant");
      break;
    case "typ9":
      var r9 = document.getElementById("r9").textContent;
      var i9 = document.getElementById("i9").textContent;

      document.getElementById("input1").value = float(r9);
      document.getElementById("input2").value = float(i9);
      drawType("constant");
      break;
  }
}

